//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DTEditor.rc
//
#define ID_BUTTON_DELETE                3
#define ID_BUTTON_MODIFY                4
#define ID_BUTTON_ADD                   5
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_DTEditorTYPE                   129
#define IDD_ATTRIBUTE                   130
#define IDD_ACCELL                      131
#define IDD_EDITSTYLE                   132
#define IDC_LIST                        1000
#define IDC_EDIT                        1001
#define IDC_EDIT_ID                     1001
#define IDC_EDIT_KEYVALUE               1002
#define IDC_CHECK01                     1002
#define IDC_CHECK02                     1003
#define IDC_LIST_KEY_NAME               1003
#define IDC_CHECK03                     1004
#define IDC_CHECK04                     1005
#define IDC_EDIT_TYPE                   1006
#define IDC_CHECK05                     1007
#define IDC_CHECK06                     1008
#define IDC_BUTTON                      1009
#define IDC_CHECK07                     1009
#define IDC_CHECK09                     1010
#define IDC_CHECK08                     1011
#define IDC_CHECK10                     1012
#define IDC_CHECK11                     1013
#define IDC_CHECK12                     1014
#define IDC_CHECK13                     1015
#define ID_BUTTON32771                  32771
#define ID_ADD_EDIT                     32772
#define ID_ADD_COMBO                    32773
#define ID_EDIT_ACCELL                  32774
#define ID_DELETE                       32775
#define ID_ASSIST_LINE                  32776
#define ID_SETTAB                       32777
#define ID_MOVE_UP                      32778
#define ID_ADD_LIST                     32779
#define ID_MOVE_DOWN                    32780
#define ID_ADD_PROGRESS                 32781
#define ID_ADD_STATIC                   32782
#define ID_MOVE_LEFT                    32783
#define ID_MOVE_RIGHT                   32784
#define ID_RESIZE_LEFT                  32785
#define ID_RESIZE_RIGHT                 32786
#define ID_RESIZE_UP                    32787
#define ID_RESIZE_DOWN                  32788
#define ID_ADD_IMAGE_BUTTON             32789
#define ID_ADD_CHECK_BOX                32792
#define ID_MENUITEM32793                32793
#define ID_ADD_BUTTON                   32796

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32794
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
