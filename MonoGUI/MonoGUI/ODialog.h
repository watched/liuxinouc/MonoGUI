// ODialog.h: interface for the ODialog class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////
#if !defined(__ODIALOG_H__)
#define __ODIALOG_H__

typedef struct _CTRLDATA
{
	WORD  wType;            // 控件类型
	WORD  wStyle;           // 控件风格
	int   x, y, w, h;       // 控件相对父窗口的位置
	int   id;               // 控件ID号
	char  caption[WINDOW_CAPTION_BUFFER_LEN];     // 控件口题头文字
	int   nAddData;         // 附加数据(OEdit中是最大字符串长度)
	_CTRLDATA*	next;       // 指向链表中的下一个
} CTRLDATA;

typedef struct _DLGTEMPLET
{
     WORD  wStyle;          // 对话框风格
     int   x, y, w, h;      // 对话框相对桌面的显示位置
     char  caption[WINDOW_CAPTION_BUFFER_LEN];    // 对话框题头文字
     int   id;              // 对话框的id号
     int   controlnr;       // 对话框附带的控件数目
     CTRLDATA*  controls;   // 控件模板链表的指针
     char*  pAccellTable;   // 快捷键列表的文本
} DLGTEMPLET;


class ODialog : public OWindow
{
private:
	enum { self_type = WND_TYPE_DIALOG };

public:
	int m_nDoModalReturn;   // 指出DoModal函数的返回值
	OAccell* m_pAccell;     // 快捷键列表

public:
	ODialog();
	virtual ~ODialog();

	// 虚函数，绘制对话框
	virtual void Paint (LCD* pLCD);

	// 虚函数，消息处理
	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);

#if defined(MOUSE_SUPPORT)
	// 坐标设备消息处理
	virtual int PtProc (OWindow* pWnd, int nMsg, int wParam, int lParam);
#endif // defined(MOUSE_SUPPORT)

	// 进入模式状态
	virtual int DoModal();

	// 根据ID号，使用相应的模板文件创建对话框
	virtual BOOL CreateFromID (OWindow* pWnd, int nID);

	// 从对话框模板创建对话框
	BOOL CreateFromTemplet (OWindow* pParent, char* filename);

	// 处理焦点切换
	virtual BOOL OnChangeFocus();

private:
	// 从模板文件初始化对话框模板
	BOOL OpenDlgTemplet (DLGTEMPLET* pTemplet, char* filename);

	// 从字符串中获取对话框设置
	BOOL GetDialogInfoFromString (DLGTEMPLET* pTemplet, char* sString);

	// 从字符串中获取控件设置
	BOOL GetControlInfoFromString (DLGTEMPLET* pTemplet, char* sString);

	// 删除对话框模板
	// 应当依照链表从最后一个开始delete，直到删光所有控件模板
	BOOL DeleteDlgTemplet (DLGTEMPLET* pTemplet);

	// 绘制对话框
	// bFocusMode：FALSE,非焦点窗口；TRUE,反白显示(焦点窗口)
	void DrawDialog (LCD* pLCD, BOOL bFocusMode);

};

#endif // !defined(__ODIALOG_H__)
