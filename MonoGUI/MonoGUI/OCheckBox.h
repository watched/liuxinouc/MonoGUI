// OCheckBox.h: interface for the OCheckBox class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__OCHECKBOX_H__)
#define __OCHECKBOX_H__


class OCheckBox : public OWindow
{
private:
	enum { self_type = WND_TYPE_CHECK_BOX };

	int m_nCheckState;

public:
	OCheckBox ();
	virtual ~OCheckBox ();

	BOOL Create (OWindow* pParent,
		WORD wStyle,
		WORD wStatus,
		int x,
		int y,
		int w,
		int h,
		int ID
	);
	
	// 虚函数，绘制按钮
	virtual void Paint (LCD* pLCD);

	// 虚函数，消息处理
	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);

#if defined (MOUSE_SUPPORT)
	// 坐标设备消息处理
	virtual int PtProc (OWindow* pWnd, int nMsg, int wParam, int lParam);
#endif // defined(MOUSE_SUPPORT)

	// 设置选择状态
	BOOL SetCheck (int nCheck);

	// 得到选择状态
	int GetCheck ();

private:
	// 处理选择状态改变
	void OnCheck ();
};

#endif // !defined(__OCHECKBOX_H__)
