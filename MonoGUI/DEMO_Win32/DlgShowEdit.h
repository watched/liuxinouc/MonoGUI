// DlgShowEdit.h
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined (__DLGSHOWEDIT_H__)
#define __DLGSHOWEDIT_H__

class CDlgShowEdit : public ODialog
{
public:
	CDlgShowEdit();
	virtual ~CDlgShowEdit();

	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);
};

#endif // !defined(__DLGSHOWEDIT_H__)