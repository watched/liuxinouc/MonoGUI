// DemoApp.cpp: implementation of the DemoApp class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if defined (RUN_ENVIRONMENT_LINUX)
#include "init.h"
#endif

#include "stdafx.h"
#include "Images.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
DemoApp::DemoApp()
{
	InitImageMgt (m_pImageMgt);
}

DemoApp::~DemoApp()
{
}

void DemoApp::PaintWindows (OWindow* pWnd)
{
	OApp::PaintWindows (pWnd);

	// 测试代码，看看buffer里面画了什么东西
	//m_pScreenBuffer->DebugSaveSnapshoot("ScreenBuffer.bmp");
	//m_pLCD->DebugSaveSnapshoot("LCD.bmp");
	//exit(0);
}

// 显示开机画面
void DemoApp::ShowStart ()
{
	m_pSysBar->SetStatus (OSystemBar::SYSBAR_STATE_BAT_SUPPLY);
	m_pSysBar->SetBattery (50);

	SetClockPos (280, 20);

#if defined (MOUSE_SUPPORT)
	SetClockButton (SCREEN_W - 40, 20);
#endif // defined(MOUSE_SUPPROT)

	// TODO：开机动画可以添加在这个位置

	return;
}

/* END */
